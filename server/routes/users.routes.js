const express = require('express');
const router = express.Router();
const User = require('../models/users.models');

// Get all users
router.get('/', async (req, res) => {
    try {
        const users = await User.find()
        res.send(users)
    } catch(error){
        res.status(500).send('Something went wrong', error)
    }
});

// Get one user
router.get('/:id', getUser, (req, res) => {
    /* res.send(res.user.username) */
    res.json(res.user)
})

// Create new user
router.post('/', async (req, res) => {
    let { username, email, password } = req.body;
    const user = new User({
        username: username,
        email: email,
        password: password
    })
		try {
			const newUser = await user.save()
			console.log ('new user added!!')
			res.status(201).send(newUser)
		}
		catch(error){
			res.status(404).send(error)
		}
    })

// Update one user
router.patch('/:id', getUser, async (req, res) => {
    if(res.user.username != null) {
        res.user.username = req.body.username
    } if(res.user.email != null) {
        res.user.email = req.body.email
      } 
      try {
        const updatedUser = await res.user.save()
        res.status(201).send(updatedUser)
      } catch(error) {
        res.status(404).send(error)
      }
})


// Delete one user
router.delete('/:id', getUser, async (req, res) => {
    try {
        await res.user.remove()
        res.send ('user deleted succesfully')
    } catch(error) {
        res.status(500).send(error)
    }
})


// middleware to get the user by its id
async function getUser(req, res, next) {
    let user = null
    try {
        user = await User.findById(req.params.id)
        if(user == null) {
            return res.status(404).send('Cannot find user')
        }
    } catch(error) {
        return res.status(500).send(error)
    }

    res.user = user
    next()
}

// router.post('/register', controller.register);
// router.post('/login', controller.login);
// router.post('/verify_token', controller.verify_token);

module.exports = router