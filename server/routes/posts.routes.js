const express = require('express');
const router = express.Router();
const Post = require('../models/posts.models')


// Get all posts
router.get('/', async (req, res) => {
    try {
        const posts = await Post.find()
        res.send(posts)
    } catch(error) {
        res.status(404).send(error)
    }
})

// Get one post by title
router.get('/:title', getPost, (req, res) => {
    /* res.send(res.post.title) */
    res.json(res.post)
})

// Create new post
router.post('/', async (req, res) => {
    let { title, text, image, signature } = req.body
    const post = new Post({
        title: title,
        text: text,
        image: image,
        signature: signature
    })
        try {
            const newPost = await post.save()
            console.log('new post added!')
            res.status(201).send(newPost)
        } catch(error) {
            res.status(404).send(error)
        }
})

// Update one post (by title)
    // write the function here

// Delete one post (by title)
router.delete('/:title', getPost, async (req, res) => {
    try {
        await res.title.Model.findOneAndRemove()
        res.send('post removed succesfully')
    } catch(error) {
        res.status(404).send(error)
    }
})

// middleware to get the post by its title
async function getPost(req, res, next) {
    let post = null
    try {
        post = await Post.findOne(req.params.title.toString())
        if (post == null) {
            return res.status(404).send('This post does not exists')
        }
    } catch(error) {
        return res.status(404).send(error)
    }

    res.post = post
    next()

}

module.exports = router