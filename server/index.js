const express = require ('express');
const app = express()
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');

const port = process.env.PORT || 5000

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

mongoose.set('useCreateIndex', true);
async function connecting() {
	try {
		await mongoose.connect('mongodb+srv://elisaeli:matildina2009@cluster0.yfoor.mongodb.net/blogDB?retryWrites=true&w=majority', { useUnifiedTopology: true, useNewUrlParser: true });
		console.log('Connected to the DB');
	} catch (error) {
		console.log('ERROR: Seems like your DB is not running, please start it up !!!');
	}
}
connecting();

const usersRouter = require('./routes/users.routes')
const postsRouter = require('./routes/posts.routes')
const commentsRouter = require('./routes/comments.routes')
app.use ('/users', usersRouter)
app.use ('/posts', postsRouter)
app.use ('/comments', commentsRouter)


// app.use(cookieParser())

// app.use(
// 	session({
// 		key: 'user_sid',
// 		secret: 'somethingrandom',
// 		resave: false,
// 		saveUninitialized: false,
// 		cookie: {
// 			expires: 600000
// 		}
// 	})
// )

// app.use((req, res, next) => {
// 	if(req.session.user && req.cookies.user_sid) {
// 		res.redirect('/posts')
// 	}
// 	next()
// })


// var sessionChecker = ((req, res, next) => {
// 	if(req.session.user && req.cookies.user_sid) {
// 		res.redirect('/posts')
// 	} else {
// 		next()
// 	}
// })

// app.get('/', sessionChecker, (req, res) => {
// 	res.redirect('/login')
// })

// // login route
// app.route('/login')
// .get(sessionChecker, (req, res) => {
// 	res.sendFile(__dirname + '/public/login.html')
// })
// .post (async (req, res) => {
// 	var username = req.body.username
// 	var password = req.body.password

// 	try{
// 		var user = await user.models.findOne({username:username}).exec()
// 		if (!user) {
// 			res.redirect('/login')
// 		}

// 		user.comparePassword(password, (err, match) => {
// 			if(!match) {
// 				res.redirect('/login')
// 			}
// 		})

// 		req.session.user
// 		res.redirect('/posts')

// 	} catch (err) {
// 		console.log(err)
// 	}
// })

// // signup route
// app.route('/signup')
// .get(sessionChecker, (req, res) => {
// 	res.sendFile(__dirname + '/public/signup.html')
// })
// .post((req, res) => {
// 	var user = new users({
// 		username: req.body.username,
// 		email: req.body.email,
// 		password: req.body.password
// 	})

// 	user.save((err, docs) => {
// 		if(err) {
// 			res.redirect('/signup')
// 		} else {
// 			console.log(docs)
// 			req.session.user = docs
// 			req.redirect('/posts')
// 		}
// 	})
// })

// // posts route
// // app.get('/posts', (req, res) => {
// // 	if(req.session.user && req.cookies.user_sid) {
// // 		res.sendFile(__dirname + './public/posts.html')
// // 	} else {
// // 		res.redirect('/login')
// // 	}
// // })

// // logout route
// app.get('/logout', (req, res) => {
// 	if(req.session.user && req.cookies.user_sid) {
// 		res.clearCookie('user_sid')
// 		res.redirect('/')
// 	} else {
// 		res.redirect('/login')
// 	}
// })


const cors = require('cors');
app.use(cors());

app.listen(port, () => {
    console.log('server is running on port', port)
})