const mongoose = require('mongoose');

// const bcrypt = require('bcrypt')

// mongoose.connect('mongodb://localhost:27017/userDB', {
// 	useNewUrlParser: true,
// 	useUnifiedTopology: true
// })

const userSchema = new mongoose.Schema({
	username: { type: String, unique: true, required: true },
	email: { type: String, unique: true, required: true },
	password: { type: String, required: true }
});


module.exports = mongoose.model('User', userSchema);