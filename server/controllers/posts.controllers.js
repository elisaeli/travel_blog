const Posts = require('../models/posts.models');

class PostsController {
	async findAll(req, res){
		try {
			const posts = await Posts.find({});
			res.send(posts)
		}
		catch(error){
			res.send('Something went wrong', error)
		}
	}

	async insert(req, res){
		let { id, title, text, image, signature } = req.body;
		try {
			const post = await Posts.create({id, title, text, image, signature});
			console.log ('post added!!')
			res.status(201).send(post)
		}
		catch(error){
			res.status(404).send(error)
		}
	}

	async update(req, res){
		let { id, title } = req.body;
		try {
			const updatePost = await Posts.updateOne({id, title, text, image, signature});
			console.log('post updated!')
			res.status(201).send(updatePost)
		}
		catch(error){
			res.statur(404).send(error)
		}
	}

	async delete(req, res){
		let { id } = req.body;
		try {
			const removed = await Posts.deleteOne( { id } );
			res.status(204).send({removed});
		}
		catch(error){
			res.status(404).send('Something went wrong', error)
		}
	}

}

module.exports = new PostsController