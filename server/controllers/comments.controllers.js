const Comments = require('../models/comments.models');

class CommentsController {
	async findAll(req, res){
		try {
			const comments = await Comments.find({});
			res.status(200).send(comments)
		}
		catch(error){
			res.status(200).send(error)
		}
	}

	async insert(req, res){
		let { description } = req.body;
		try {
			const comment = await Comments.create({description});
			console.log ('comment added!!')
			res.status(201).send(comment)
		}
		catch(error){
			res.status(404).send(error)
		}
	}

	async update(req, res){
		let { description } = req.body;
		try {
			const updateComment = await Comments.update({description});
			console.log('comment updated!')
			res.status(201).send(updateComment)
		}
		catch(error){
			res.statur(404).send(error)
		}
	}


	async delete(req, res){
		let { description } = req.body;
		try {
			const removedComm = await Comments.deleteOne( { description } );
			res.status(204).send({removedComm});
		}
		catch(error){
			res.status(404).send('Something went wrong', error)
		}
	}
}

module.exports = new CommentsController